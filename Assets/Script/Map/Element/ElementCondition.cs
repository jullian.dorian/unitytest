﻿using System;
using System.Collections.Generic;
using Script.Utils;
using UnityEditor;
using UnityEngine;

namespace Script.Map.Element
{
    [Serializable]
    public class ElementCondition
    {

        [Tooltip("On accepte certain Elements et on refuse tout le reste.")]
        public List<ElementGenerator> whiteList = new List<ElementGenerator>();
        [Tooltip("On refuse certain Elements et on accepte tout le reste.")]
        public List<ElementGenerator> blackList = new List<ElementGenerator>();

        [Tooltip("Si la condition est 'Accept' alors whitelist & blacklist ne doivent pas comporter l'element X;" +
                 "Si la condition est 'Refuse' alors whitelist & blacklist doivent comporter l'element X;" +
                 "Si la condition est 'None' on ignore tout sauf les portes")]
        public Condition condition = Condition.NONE;
    }
}