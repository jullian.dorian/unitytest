﻿using System;
using Unity.VisualScripting;
using UnityEngine;

namespace Script.Map.Element
{
    public class ElementGenerator : MonoBehaviour
    {
        [Tooltip("Si l'élement ne peut être posé seulement sur un mur, a contrario, uniquement sur le sol.")]
        public bool onlyWall = false;
        [Tooltip("Si l'élement peut superposer un autre élement. (Ex: Toile d'araignée)")]
        public bool superposeElement = false;
        [Tooltip("On accepte que l'élement puisse avoir une superposition. (Ex: Coffre superposé par une toile)")]
        public bool acceptSuperpose = false;
        [Tooltip("Si l'element peut apparaitre dans un couloir.")]
        public bool appearCorridor = false;
        [Range(-1, 42)]
        [Tooltip("Le nombre maximum que peut apparaitre l'element dans une salle")]
        public int maxGenerateInRoom = -1;
        [Range(1, 100)]
        [Tooltip("Si le nombre est '-1' on peut effectuer un pourcentage d'apparition pour la salle.")]
        public int percentageGenerate = 100;
        
        public ElementCondition[] elementConditions = Arrays<ElementCondition>.Fill(8, new ElementCondition());
    }
}