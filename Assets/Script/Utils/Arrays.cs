﻿public class Arrays<T>
{

    public static T[] Fill(int size, T o)
    {
        T[] arrays = new T[size];

        for (var i = 0; i < size; i++)
        {
            arrays[i] = o;
        }
        
        return arrays;
    } 
    
}
