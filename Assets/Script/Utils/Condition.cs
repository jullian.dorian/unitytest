﻿using System;

namespace Script.Utils
{
    [Serializable]
    public enum Condition
    {
        ACCEPT,
        REFUSE,
        NONE
    }
}