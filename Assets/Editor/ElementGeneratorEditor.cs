﻿using System;
using System.Collections.Generic;
using Script.Map.Element;
using Script.Utils;
using UnityEditor;
using UnityEngine;

namespace Editor
{
    [CustomEditor(typeof(ElementGenerator))]
    public class ElementGeneratorEditor : UnityEditor.Editor
    {

        private SerializedProperty _onlyWall;
        private SerializedProperty _superposeElement;
        private SerializedProperty _acceptSuperpose;
        private SerializedProperty _appearCorridor;
        private SerializedProperty _maxGenerateInRoom;
        private SerializedProperty _percentageGenerate;

        private List<SerializedProperty> _gridConditions = new List<SerializedProperty>();
        
        private void OnEnable()
        {
            _onlyWall = serializedObject.FindProperty("onlyWall");
            _superposeElement = serializedObject.FindProperty("superposeElement");
            _acceptSuperpose = serializedObject.FindProperty("acceptSuperpose");
            _appearCorridor = serializedObject.FindProperty("appearCorridor");
            _maxGenerateInRoom = serializedObject.FindProperty("maxGenerateInRoom");
            _percentageGenerate = serializedObject.FindProperty("percentageGenerate");
            var conditions = serializedObject.FindProperty("elementConditions");

            for (var i = 0; i < conditions.arraySize; i++)
            {
                var cond = conditions.GetArrayElementAtIndex(i);
                _gridConditions.Add(cond);
            }

            Debug.Log(conditions.arraySize);
            Debug.Log(_gridConditions.Count);
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;
            
            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(_onlyWall);
            EditorGUILayout.PropertyField(_superposeElement);
            EditorGUILayout.PropertyField(_acceptSuperpose);
            EditorGUILayout.PropertyField(_appearCorridor);
            EditorGUILayout.PropertyField(_maxGenerateInRoom);
            EditorGUILayout.PropertyField(_percentageGenerate);
            
            EditorGUI.EndChangeCheck();
            EditorGUI.indentLevel = indent;

            //Au debut ou à la fin ça change rien par rapport à la fenetre
            if (serializedObject.ApplyModifiedProperties())
            {
                Debug.Log("Objet modifié sauvegarde");
            }
            
            EditorGUILayout.Separator();
            
            float boxSizeCondition = 42;
            Rect position = EditorGUILayout.GetControlRect(true, boxSizeCondition * 3 + 4 * 3);
            var boxCondition = new Rect(position.x + 32, position.y, position.width - 16, position.height);

            short countCondIndex = 0;
            for (int y = 0; y < 3; y++)
            {
                for (int x = 0; x < 3; x++)
                {
                    var box = new Rect(boxCondition.x + ((boxSizeCondition + 4) * x), boxCondition.y + ((boxSizeCondition + 4) * y), boxSizeCondition, boxSizeCondition);
                    var elementCondition = _gridConditions[countCondIndex];

                    SerializedProperty condition = elementCondition.FindPropertyRelative("condition");
                    
                    if (y != 1 || x != 1)
                    {
                        var tileCond = (Condition) condition.intValue;
                        Color color = tileCond == Condition.NONE ? Color.gray : tileCond == Condition.ACCEPT ? Color.green : Color.red;
                        
                        //On place les box
                        EditorGUI.DrawRect(box, color);
                        GUI.BeginGroup(box);
                        if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
                        {
                            ElementConditionWindowEditor.ShowWindow(serializedObject, elementCondition);
                        }
                        GUI.EndGroup();


                        countCondIndex++;
                    }
                    else
                    {
                       //Afficher le centre avec son image/autre
                    }
                }
            }
            
        }
    }
}