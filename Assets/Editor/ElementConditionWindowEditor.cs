﻿using UnityEditor;
using UnityEngine;

public class ElementConditionWindowEditor : EditorWindow
{

    private SerializedObject _serializedObject;
    private SerializedProperty _elementCondition;
    UnityEditor.Editor editor;
    
    public static void ShowWindow(SerializedObject serializedObject, SerializedProperty serializedProperty)
    {
        var window = GetWindow<ElementConditionWindowEditor>();
        window.titleContent = new GUIContent("Modifier les conditions");
        window._serializedObject = serializedObject;
        window._elementCondition = serializedProperty;
        window.Show();
    }

    private void OnGUI()
    {
        Debug.Log("test");
        
        if (!editor) { editor = UnityEditor.Editor.CreateEditor(this); }
        if (editor) { editor.OnInspectorGUI(); }
        
        SerializedProperty whiteList = _elementCondition.FindPropertyRelative("whiteList");
        SerializedProperty blackList = _elementCondition.FindPropertyRelative("blackList");
        SerializedProperty condition = _elementCondition.FindPropertyRelative("condition");

        //var indent = EditorGUI.indentLevel;
        //EditorGUI.indentLevel = 1;
        
        //_serializedObject.Update();
        
        EditorGUILayout.PropertyField(whiteList);
        EditorGUILayout.PropertyField(blackList);
        EditorGUILayout.PropertyField(condition);

        //_serializedObject.ApplyModifiedProperties();
        
        //EditorGUI.indentLevel = indent;
    }
}
